(use-package rust-mode)
(use-package rust-auto-use)
(use-package rustic)
(use-package cargo-mode)
(use-package cargo)

(setq rust-indent-offset 2)

(provide 'setup-rust-mode)
